$(function(){
    const socket = io();
    $('form').submit(function(e){
        e.preventDefault(); //évite que la page ne se recharge
        socket.emit('chat message', $("#m").val());
        $('#m').val('')
        return false;
    });
    socket.on('chat message', (msg)=> {
        $('#messages').append($('<li>').text(msg));
    });
});