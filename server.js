const express = require('express');
const app = express();
let path = require('path')
const http = require('http').createServer(app);
const io = require('socket.io')(http);

app.use(express.static(path.join(__dirname, 'public')))

// app.get('/', (req,res)=> {
//     res.sendFile(__dirname + '/public/index.html');
// });

io.on('connection', (socket) => {
    socket.broadcast.emit('chat message' , 'un utilisateur s\'est connecté');

    socket.on('chat message', (msg) => {
        io.emit('chat message', msg);
    });

    socket.on('disconnect', () => {
        socket.broadcast.emit('chat message', 'un utilisateur s\'est déconnecté');
    })

});


http.listen(3031, () => {
    console.log("ok sur 3031");
})